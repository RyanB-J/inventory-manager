//*******************************************************************
// AddPartController
//
// Controls the Add Part Screen from AddPart.fxml
//*******************************************************************

package scenes;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

import java.io.IOException;

public class AddPartController {

    @FXML private RadioButton inhouseRadio;
    @FXML private RadioButton outsourcedRadio;
    @FXML private TextField idField;
    @FXML private TextField nameField;
    @FXML private TextField stockField;
    @FXML private TextField priceField;
    @FXML private TextField maxField;
    @FXML private TextField minField;
    @FXML private TextField compMachField;
    @FXML private Text compMachLabel;

    // Inventory Object
    private Inventory inventory = Main.getInventory();

    // Radio Selection
    private boolean isOutsourced = true;

    // Cancel Confirmation Alert
    private Alert cancelConfirmationAlert = new Alert(Alert.AlertType.CONFIRMATION,
            "Are you sure?",
            ButtonType.YES, ButtonType.NO);

    // Exception check the form for impossible data
    private void checkForm() {
        int stock = (!stockField.getText().equals("") ? Integer.parseInt(stockField.getText()) : 0);
        int min = (!minField.getText().equals("") ? Integer.parseInt(minField.getText()) : 0);
        int max = (!maxField.getText().equals("") ? Integer.parseInt(maxField.getText()) : 0);

        if (stock < min || stock > max) {
            throw new RuntimeException("Inventory value exceeds the minimum or maximum value.");
        }
    }

    // In-House Radio
    @FXML
    private void inhouseRadioSelected(ActionEvent event) {

        // Change Company Name Field to Machine ID Field
        if (isOutsourced) {
            compMachLabel.setText("MachineID");
            compMachField.setPromptText("Mach ID");
            isOutsourced = false;
        }
    }

    // Outsourced Radio
    @FXML
    private void outsourcedRadioSelected(ActionEvent event) {

        // Change Machine ID Field to Company Name Field
        if (!isOutsourced) {
            compMachLabel.setText("Company Name");
            compMachField.setPromptText("Comp Nm");
            isOutsourced = true;
        }
    }

    // Save Part Button
    @FXML
    private void savePart(ActionEvent event) {

        checkForm();

        try {

            // Create a new part and add it to the allParts array
            if (isOutsourced) {
                Outsourced newOutsourcedPart = new Outsourced(
                        Main.getNextPartNum(),
                        nameField.getText(),
                        (!priceField.getText().equals("") ? Double.parseDouble(priceField.getText()) : 0.0),
                        (!stockField.getText().equals("") ? Integer.parseInt(stockField.getText()) : 0),
                        (!minField.getText().equals("") ? Integer.parseInt(minField.getText()) : 0),
                        (!maxField.getText().equals("") ? Integer.parseInt(maxField.getText()) : 0),
                        compMachField.getText()
                );
                inventory.addPart(newOutsourcedPart);

            } else {
                InHouse newInHousePart = new InHouse(
                        Main.getNextPartNum(),
                        nameField.getText(),
                        (!priceField.getText().equals("") ? Double.parseDouble(priceField.getText()) : 0.0),
                        (!stockField.getText().equals("") ? Integer.parseInt(stockField.getText()) : 0),
                        (!minField.getText().equals("") ? Integer.parseInt(minField.getText()) : 0),
                        (!maxField.getText().equals("") ? Integer.parseInt(maxField.getText()) : 0),
                        (!compMachField.getText().equals("") ? Integer.parseInt(compMachField.getText()) : 0)
                );
                inventory.addPart(newInHousePart);
            }

            Main.handleSceneChange("Inventory.fxml");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    // Cancel Part Button
    @FXML
    private void cancelPart(ActionEvent event) {
        cancelConfirmationAlert.showAndWait();
        if (cancelConfirmationAlert.getResult() == ButtonType.YES) {
            try {
                Main.handleSceneChange("Inventory.fxml");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
