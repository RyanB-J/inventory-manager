//*******************************************************************
// ModifyPartController
//
// Controls the Modify Part Screen from ModifyPart.fxml
//*******************************************************************

package scenes;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;


import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ModifyPartController implements Initializable {

    // Text Fields
    @FXML private RadioButton inhouseRadio;
    @FXML private RadioButton outsourcedRadio;
    @FXML private TextField idField;
    @FXML private TextField nameField;
    @FXML private TextField stockField;
    @FXML private TextField priceField;
    @FXML private TextField maxField;
    @FXML private TextField minField;
    @FXML private TextField compMachField;
    @FXML private Text compMachLabel;

    // Instance Variables
    private Inventory inventory = Main.getInventory();
    private Part selectedPart;
    private int selectedPartIndex;

    // Radio Selection
    private boolean isOutsourced = true;

    // Cancel Confirmation Alert
    private Alert cancelConfirmationAlert = new Alert(Alert.AlertType.CONFIRMATION,
            "Are you sure?",
            ButtonType.YES, ButtonType.NO);

    // Exception check the form for impossible data
    private void checkForm() {
        int stock = (!stockField.getText().equals("") ? Integer.parseInt(stockField.getText()) : 0);
        int min = (!minField.getText().equals("") ? Integer.parseInt(minField.getText()) : 0);
        int max = (!maxField.getText().equals("") ? Integer.parseInt(maxField.getText()) : 0);

        if (stock < min || stock > max) {
            throw new RuntimeException("Inventory value exceeds the minimum or maximum value.");
        }
    }

    // Instructions to change the scene to an Inhouse part
    private void setSceneToInHouse() {
        compMachLabel.setText("MachineID");
        compMachField.setPromptText("Mach ID");

        // Ensure that the radio option is checked
        if (!inhouseRadio.isSelected()) {
            inhouseRadio.fire();
        }
        isOutsourced = false;
    }

    // Instructions to change the scene to an Outsourced part
    private void setSceneToOutsourced() {
        compMachLabel.setText("Company Name");
        compMachField.setPromptText("Comp Nm");

        // Ensure that the radio option is checked (Always False if Outsource is Default)
        if (!outsourcedRadio.isSelected()) {
            inhouseRadio.fire();
        }
        isOutsourced = true;
    }

    // In-House Radio
    @FXML
    private void inhouseRadioSelected(ActionEvent event) {

        // Change Company Name Field to Machine ID Field
        if (isOutsourced) {
            setSceneToInHouse();
        }
    }

    // Outsourced Radio
    @FXML
    private void outsourcedRadioSelected(ActionEvent event) {

        // Change Machine ID Field to Company Name Field
        if (!isOutsourced) {
            setSceneToOutsourced();
        }
    }

    // Save Part Button
    @FXML
    private void savePart(ActionEvent event) {

        checkForm();

        try {

            if (isOutsourced) {
                Outsourced newOutsourcedPart = new Outsourced(
                        selectedPart.getId(),
                        nameField.getText(),
                        (!priceField.getText().equals("") ? Double.parseDouble(priceField.getText()) : 0.0),
                        (!stockField.getText().equals("") ? Integer.parseInt(stockField.getText()) : 0),
                        (!minField.getText().equals("") ? Integer.parseInt(minField.getText()) : 0),
                        (!maxField.getText().equals("") ? Integer.parseInt(maxField.getText()) : 0),
                        compMachField.getText()
                );
                inventory.updatePart(selectedPartIndex, newOutsourcedPart);

            } else {
                InHouse newInHousePart = new InHouse(
                        selectedPart.getId(),
                        nameField.getText(),
                        (!priceField.getText().equals("") ? Double.parseDouble(priceField.getText()) : 0.0),
                        (!stockField.getText().equals("") ? Integer.parseInt(stockField.getText()) : 0),
                        (!minField.getText().equals("") ? Integer.parseInt(minField.getText()) : 0),
                        (!maxField.getText().equals("") ? Integer.parseInt(maxField.getText()) : 0),
                        (!compMachField.getText().equals("") ? Integer.parseInt(compMachField.getText()) : 0)
                );
                inventory.updatePart(selectedPartIndex, newInHousePart);
            }

            Main.handleSceneChange("Inventory.fxml");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    // Cancel Part Button
    @FXML
    private void cancelPart(ActionEvent event) {
        cancelConfirmationAlert.showAndWait();
        if (cancelConfirmationAlert.getResult() == ButtonType.YES) {
            try {
                Main.handleSceneChange("Inventory.fxml");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // Instantiate the part to the selected part
        selectedPart = InventoryController.getSelectedPart();
        selectedPartIndex = InventoryController.getSelectedPartIndex();

        // Set the Values of the ModifyPart Scene to the Selected Part's Values
        idField.setText(Integer.toString(selectedPart.getId()));
        nameField.setText(selectedPart.getName());
        stockField.setText(Integer.toString(selectedPart.getStock()));
        priceField.setText(Double.toString(selectedPart.getPrice()));
        maxField.setText(Integer.toString(selectedPart.getMax()));
        minField.setText(Integer.toString(selectedPart.getMin()));

        if (selectedPart instanceof Outsourced) {
            compMachField.setText(((Outsourced) selectedPart).getCompanyName());
        } else {
            setSceneToInHouse();
            compMachField.setText(Integer.toString(((InHouse) selectedPart).getMachineId()));
        }
    }
}
