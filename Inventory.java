//*******************************************************************
// Inventory
//
// Class for the Inventory Object and associated methods
//*******************************************************************

package scenes;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Inventory {

    private ObservableList<Part> allParts = FXCollections.observableArrayList();
    private ObservableList<Product> allProducts = FXCollections.observableArrayList();

    public void addPart(Part newPart) {
        allParts.add(newPart);
    }

    public void addProduct(Product newProduct) { allProducts.add(newProduct); }

    public Part lookupPart(int partId) {
        Part part = null;
        for (Part item:allParts) {
            if (item.getId() == partId) {
                part = item;
                break;
            }
        }
        return part;
    }

    public Product lookupProduct(int productId) {
        Product product = null;
        for (Product item:allProducts) {
            if (item.getId() == productId) {
                product = item;
                break;
            }
        }
        return product;
    }

    public ObservableList<Part> lookupPart(String partName) {

        // Create an array of results that match the search field
        ObservableList<Part> queryMatches = FXCollections.observableArrayList();
        for (Part part:getAllParts()) {
            if (part.getName().toLowerCase().contains(partName.toLowerCase())) {
                queryMatches.add(part);
            }
        }

        return queryMatches;
    }

    public ObservableList<Product> lookupProduct(String productName) {

        // Create an array of results that match the search field
        ObservableList<Product> queryMatches = FXCollections.observableArrayList();
        for (Product product:getAllProducts()) {
            if (product.getName().toLowerCase().contains(productName.toLowerCase())) {
                queryMatches.add(product);
            }
        }

        return queryMatches;
    }

    public void updatePart(int index, Part selectedPart) { allParts.set(index, selectedPart); }

    public void updateProduct(int index, Product selectedProduct) { allProducts.set(index, selectedProduct); }

    public boolean deletePart(Part selectedPart) { return allParts.remove(selectedPart); }

    public boolean deleteProduct(Product selectedProduct) { return allProducts.remove(selectedProduct); }

    public ObservableList<Part> getAllParts() { return allParts; }

    public ObservableList<Product> getAllProducts() { return allProducts; }
}
