//*******************************************************************
// ModifyProductController
//
// Controls the Modify Product Screen from ModifyProduct.fxml
//*******************************************************************

package scenes;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;

import java.io.IOException;
import java.net.URL;
import java.text.NumberFormat;
import java.util.*;

public class ModifyProductController implements Initializable {

    @FXML private TextField partSearchField;
    @FXML private TextField idField;
    @FXML private TextField nameField;
    @FXML private TextField stockField;
    @FXML private TextField priceField;
    @FXML private TextField maxField;
    @FXML private TextField minField;
    @FXML private Button partSearchButton;

    // Hook up the Parts Table
    @FXML private TableView<Part> partTableView;
    @FXML private TableColumn<Part, Integer> partIdColumn;
    @FXML private TableColumn<Part, String> partNameColumn;
    @FXML private TableColumn<Part, Integer> partStockColumn;
    @FXML private TableColumn<Part, Double> partPriceColumn;

    // Hook up the Associated Parts Table
    @FXML private TableView<Part> associatedPartTableView;
    @FXML private TableColumn<Part, Integer> associatedPartIdColumn;
    @FXML private TableColumn<Part, String> associatedPartNameColumn;
    @FXML private TableColumn<Part, Integer> associatedPartStockColumn;
    @FXML private TableColumn<Part, Double> associatedPartPriceColumn;

    // Instance Variables
    private Inventory inventory = Main.getInventory();
    private Product product;
    private int productIndex;

    // Temporary Arrays
    private ObservableList<Part> partTableArray = FXCollections.observableArrayList();
    private ObservableList<Part> associatedPartTableArray = FXCollections.observableArrayList();

    // Selected Table Values
    private static Part selectedPart;
    private static Part selectedAssociatedPart;

    // Cancel Confirmation Alert
    private Alert cancelConfirmationAlert = new Alert(Alert.AlertType.CONFIRMATION,
            "Are you sure?",
            ButtonType.YES, ButtonType.NO);

    // Textfield Key Listeners
    @FXML
    private void partSearchFieldKeyHandler(KeyEvent event) {
        if (event.getCode().toString().equals("ENTER")) {
            partSearchButton.fire();
        }
    }

    // Get a part by its ID (same as lookupPart(int) in Inventory)
    private Part getPartById(int partId, ObservableList<Part> array) {
        Part part = null;
        for (Part item:array) {
            if (item.getId() == partId) {
                part = item;
                break;
            }
        }
        return part;
    }

    // Get a part by its String (same as lookupPart(String) in Inventory)
    private ObservableList<Part> getPartByString(String partName) {

        // Create an array of results that match the search field
        ObservableList<Part> queryMatches = FXCollections.observableArrayList();
        for (Part part:partTableArray) {
            if (part.getName().toLowerCase().contains(partName.toLowerCase())) {
                queryMatches.add(part);
            }
        }
        return queryMatches;
    }

    // Exception check the form for impossible data
    private void checkForm() {
        int stock = (!stockField.getText().equals("") ? Integer.parseInt(stockField.getText()) : 0);
        int min = (!minField.getText().equals("") ? Integer.parseInt(minField.getText()) : 0);
        int max = (!maxField.getText().equals("") ? Integer.parseInt(maxField.getText()) : 0);

        if (stock < min || stock > max) {
            throw new RuntimeException("Inventory value exceeds the minimum or maximum value.");
        }
    }

    // Search Parts Button
    @FXML
    private void lookupPart(ActionEvent event) {
        String query = partSearchField.getText();
        ObservableList<Part> queryMatches;

        if (query != null && !query.isEmpty()) {

            if (query.matches(".*[a-zA-Z].*")) {
                queryMatches = getPartByString(query);
            } else {
                int intQuery = Integer.parseInt(query);
                queryMatches = FXCollections.observableArrayList();
                if (getPartById(intQuery, partTableArray) != null) {
                    queryMatches.add(getPartById(intQuery, partTableArray));
                }
            }

            // Change the parts table to show query matches
            partTableView.setItems(queryMatches);
            System.out.println(query + " was entered in the Part Search");

        } else {

            // Change the parts table to show all parts
            partTableView.setItems(partTableArray);
            System.out.println("Part Search Button was pressed");
        }
    }

    // Add Associated Part Button
    @FXML
    private void addAssociatedPart(ActionEvent event) {
        if (partTableView.getSelectionModel().getSelectedItem() != null) {

            System.out.println(selectedPart.getName() + " added to Associated Parts");

            // Add the selected part to the associated parts table
            product.addAssociatedPart(selectedPart);

            // Move the part from the part array to the associated part array
            associatedPartTableArray.add(getPartById(selectedPart.getId(), partTableArray));
            partTableArray.remove(getPartById(selectedPart.getId(), associatedPartTableArray));

            // Sort the tables by ID
            partTableView.sort();
            associatedPartTableView.sort();

        } else System.out.println("No Part Selected");
    }

    // Delete Associated Part Button
    @FXML
    private void deleteAssociatedPart(ActionEvent event) {
        if (associatedPartTableView.getSelectionModel().getSelectedItem() != null) {

            System.out.println(selectedAssociatedPart.getName() + " deleted from Associated Parts");

            // Move the part from the associated part array to the part array
            partTableArray.add(getPartById(selectedAssociatedPart.getId(), associatedPartTableArray));
            associatedPartTableArray.remove(getPartById(selectedAssociatedPart.getId(), associatedPartTableArray));

            // Sort the tables by ID
            partTableView.sort();
            associatedPartTableView.sort();

            // Remove the selected part from the associated parts table
            product.deleteAssociatedPart(selectedAssociatedPart);

        } else System.out.println("No Associated Part Selected");
    }

    // Save Product Button
    @FXML
    private void saveProduct(ActionEvent event) {

        checkForm();

        try {

            // Set the text field values to the instantiated product
            product.setName(nameField.getText());
            product.setPrice(((!priceField.getText().equals("") ? Double.parseDouble(priceField.getText()) : 0.0)));
            product.setStock((!stockField.getText().equals("") ? Integer.parseInt(stockField.getText()) : 0));
            product.setMin((!minField.getText().equals("") ? Integer.parseInt(minField.getText()) : 0));
            product.setMax((!maxField.getText().equals("") ? Integer.parseInt(maxField.getText()) : 0));

            // Set all of the values from the associatedPartArray into the product's associated parts
            product.getAllAssociatedParts().setAll(associatedPartTableArray);

            // Update the product in the inventory
            inventory.updateProduct(productIndex, product);

            Main.handleSceneChange("Inventory.fxml");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    // Cancel Product Button
    @FXML
    private void cancelProduct(ActionEvent event) {
        cancelConfirmationAlert.showAndWait();
        if (cancelConfirmationAlert.getResult() == ButtonType.YES) {
            try {
                Main.handleSceneChange("Inventory.fxml");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // Instantiate the product to the selected product
        product = InventoryController.getSelectedProduct();
        productIndex = InventoryController.getSelectedProductIndex();

        // Set the Values of the ModifyProduct Scene to the Selected Product's Values
        idField.setText(Integer.toString(product.getId()));
        nameField.setText(product.getName());
        stockField.setText(Integer.toString(product.getStock()));
        priceField.setText(Double.toString(product.getPrice()));
        maxField.setText(Integer.toString(product.getMax()));
        minField.setText(Integer.toString(product.getMin()));

        // Set up the Columns in the Parts Table
        partIdColumn.setCellValueFactory(new PropertyValueFactory<Part, Integer>("id"));
        partNameColumn.setCellValueFactory(new PropertyValueFactory<Part, String>("name"));
        partStockColumn.setCellValueFactory(new PropertyValueFactory<Part, Integer>("stock"));
        partPriceColumn.setCellValueFactory(new PropertyValueFactory<Part, Double>("price"));

        // Set up the Columns in the Associated Parts Table
        associatedPartIdColumn.setCellValueFactory(new PropertyValueFactory<Part, Integer>("id"));
        associatedPartNameColumn.setCellValueFactory(new PropertyValueFactory<Part, String>("name"));
        associatedPartStockColumn.setCellValueFactory(new PropertyValueFactory<Part, Integer>("stock"));
        associatedPartPriceColumn.setCellValueFactory(new PropertyValueFactory<Part, Double>("price"));

        // Convert Prices to a Currency
        NumberFormat currencyInstance = NumberFormat.getCurrencyInstance();

        partPriceColumn.setCellFactory(partPriceColumn -> new TableCell<Part, Double>() {

            @Override
            protected void updateItem(Double price, boolean empty) {
                super.updateItem(price,empty);
                if (empty) {
                    setText(null);
                } else {
                    setText(currencyInstance.format(price));
                }
            }
        });

        associatedPartPriceColumn.setCellFactory(associatedPartPriceColumn -> new TableCell<Part, Double>() {

            @Override
            protected void updateItem(Double price, boolean empty) {
                super.updateItem(price,empty);
                if (empty) {
                    setText(null);
                } else {
                    setText(currencyInstance.format(price));
                }
            }
        });

        // Add all Parts to the table view but remove associated parts
        partTableArray.setAll(inventory.getAllParts());
        associatedPartTableArray.setAll(product.getAllAssociatedParts());

        ListIterator<Part> partIterator = partTableArray.listIterator();
        ListIterator<Part> associatedPartIterator = product.getAllAssociatedParts().listIterator();

        while (partIterator.hasNext()  && !product.getAllAssociatedParts().isEmpty()) {

            Part part = partIterator.next();
            associatedPartIterator = product.getAllAssociatedParts().listIterator();

            while (associatedPartIterator.hasNext()) {

                Part associatedPart = associatedPartIterator.next();
                if (part == associatedPart) {

                    partIterator.remove();

                }
            }
        }

        // Add the Parts and Associated Parts to their Associated Tables
        partTableView.setItems(partTableArray);
        associatedPartTableView.setItems(associatedPartTableArray);

        // Sort the Parts table by Id
        partIdColumn.setSortType(TableColumn.SortType.ASCENDING);
        partTableView.getSortOrder().add(partIdColumn);
        partIdColumn.setSortable(true);

        // Sort the Associated Parts table by Id
        associatedPartIdColumn.setSortType(TableColumn.SortType.ASCENDING);
        associatedPartTableView.getSortOrder().add(partIdColumn);
        associatedPartIdColumn.setSortable(true);

        // Part Table View Selection Listener
        partTableView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Part>() {
            @Override
            public void changed(ObservableValue<? extends Part> observable, Part oldValue, Part newValue) {
                if (partTableView.getSelectionModel().getSelectedItem() != null) {

                    // Set the selectedPart Variable
                    selectedPart = newValue;
                }
            }
        });

        // Associated Part Table View Selection Listener
        associatedPartTableView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Part>() {
            @Override
            public void changed(ObservableValue<? extends Part> observable, Part oldValue, Part newValue) {
                if(associatedPartTableView.getSelectionModel().getSelectedItem() != null) {

                    // Set the selectedPart Variable
                    selectedAssociatedPart = newValue;
                }
            }
        });
    }
}
