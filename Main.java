///////////////////////////////////////////////////////////////////////////////
//
// Title:   Inventory Manager
// Course:  C482 - Software 1
// Author:  Ryan Bains-Jordan
// Email:   rbains1@wgu.edu
//
///////////////////////////////////////////////////////////////////////////////

package scenes;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;

public class Main extends Application {

    public static Stage stage;
    private static Inventory inventory = new Inventory();

    private static int generatedPartNum = 0;
    private static int generatedProductNum = 0;

    public static Alert exitConfirmationAlert = new Alert(Alert.AlertType.CONFIRMATION,
            "Are you sure? Progress will not be saved.",
            ButtonType.YES, ButtonType.CANCEL);

    // General scene changer for all scenes
    public static void handleSceneChange(String FXMLFile) throws IOException {

        Parent viewFile = FXMLLoader.load(InventoryController.class.getResource(FXMLFile));
        Scene scene = new Scene(viewFile);

        Stage stage = getStage();
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();

        // Alert for closing the window if there is at least one Part or Product in the inventory
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                if (!inventory.getAllParts().isEmpty() || !inventory.getAllProducts().isEmpty()) {
                    exitConfirmationAlert.showAndWait();
                    if (exitConfirmationAlert.getResult() == ButtonType.YES) {
                        Platform.exit();
                    } else event.consume();
                } else Platform.exit();
            }
        });
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("Inventory.fxml"));

        Scene scene = new Scene(root);
        stage = primaryStage;
        stage.setTitle("Inventory Management");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }

    public static Stage getStage() {
        return stage;
    }

    public static Inventory getInventory() { return inventory; }

    public static int getNextPartNum() { return ++generatedPartNum; }

    public static int getNextProductNum() { return ++generatedProductNum; }

    public static void main(String[] args) {
        launch(args);
    }
}
