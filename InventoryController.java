//*******************************************************************
// InventoryController
//
// Controls the (Main) Inventory Screen from Inventory.fxml
//*******************************************************************

package scenes;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;

import java.io.IOException;
import java.net.URL;
import java.text.NumberFormat;
import java.util.ResourceBundle;

public class InventoryController implements Initializable {

    // Components
    @FXML private TextField partSearchField;
    @FXML private TextField productSearchField;
    @FXML private Button partSearchButton;
    @FXML private Button productSearchButton;

    // Hook up the Parts Table
    @FXML private TableView<Part> partTableView;
    @FXML private TableColumn<Part, Integer> partIdColumn;
    @FXML private TableColumn<Part, String> partNameColumn;
    @FXML private TableColumn<Part, Integer> partStockColumn;
    @FXML private TableColumn<Part, Double> partPriceColumn;

    // Configure the Products Table
    @FXML private TableView<Product> productTableView;
    @FXML private TableColumn<Product, Integer> productIdColumn;
    @FXML private TableColumn<Product, String> productNameColumn;
    @FXML private TableColumn<Product, Integer> productStockColumn;
    @FXML private TableColumn<Product, Double> productPriceColumn;

    // Inventory Object
    private Inventory inventory = Main.getInventory();

    // Selected Table Values
    private static Part selectedPart;
    private static int selectedPartIndex;
    private static Product selectedProduct;
    private static int selectedProductIndex;

    // Getters
    public static Part getSelectedPart() {
        return selectedPart;
    }
    public static int getSelectedPartIndex() { return selectedPartIndex; }
    public static int getSelectedProductIndex() { return selectedProductIndex; }
    public static Product getSelectedProduct() { return selectedProduct; }

    // Part Textfield Key Listener
    @FXML
    private void partSearchFieldKeyHandler(KeyEvent event) {
        if (event.getCode().toString().equals("ENTER")) {
            partSearchButton.fire();
        }
    }

    // Product Textfield Key Listener
    @FXML
    private void productSearchFieldKeyHandler(KeyEvent event) {
        if (event.getCode().toString().equals("ENTER")) {
            productSearchButton.fire();
        }
    }

    // Delete Confirmation Alert
    private Alert partDeleteConfirmationAlert = new Alert(Alert.AlertType.CONFIRMATION,
            "Are you sure you want to delete this Part? This action can not be undone.",
            ButtonType.YES, ButtonType.NO);

    // Product Delete Confirmation Alert
    private Alert productDeleteConfirmationAlert = new Alert(Alert.AlertType.CONFIRMATION,
            "Are you sure you want to delete this Product? This action can not be undone.",
            ButtonType.YES, ButtonType.NO);

    // Add Part Button
    @FXML
    private void addPart(ActionEvent event) {
        try {
            Main.handleSceneChange("AddPart.fxml");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    // Add Product Button
    @FXML
    private void addProduct(ActionEvent event) {
        try {
            Main.handleSceneChange("AddProduct.fxml");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    // Search Parts Button
    @FXML
    private void lookupPart(ActionEvent event) {
        String query = partSearchField.getText();
        ObservableList<Part> queryMatches;

        if (query != null && !query.isEmpty()) {

            if (query.matches(".*[a-zA-Z].*")) {
                queryMatches = inventory.lookupPart(query);
            } else {
                int intQuery = Integer.parseInt(query);
                queryMatches = FXCollections.observableArrayList();
                if (inventory.lookupPart(intQuery) != null) {
                    queryMatches.add(inventory.lookupPart(intQuery));
                }
            }

            // Change the parts table to show query matches
            partTableView.setItems(queryMatches);
            System.out.println(query + " was entered in the Part Search");

        } else {

            // Change the parts table to show all parts
            partTableView.setItems(inventory.getAllParts());
            System.out.println("Part Search Button was pressed");
        }
    }

    // Search Products Button
    @FXML
    private void lookupProduct(ActionEvent event) {
        String query = productSearchField.getText();
        ObservableList<Product> queryMatches;

        if (query != null && !query.isEmpty()) {

            if (query.matches(".*[a-zA-Z].*")) {
                queryMatches = inventory.lookupProduct(query);
            } else {
                int intQuery = Integer.parseInt(query);
                queryMatches = FXCollections.observableArrayList();
                if (inventory.lookupProduct(intQuery) != null) {
                    queryMatches.add(inventory.lookupProduct(intQuery));
                }
            }

            // Change the products table to show query matches
            productTableView.setItems(queryMatches);
            System.out.println(query + " was entered in the Product Search");

        } else {

            // Change the products table to show all parts
            productTableView.setItems(inventory.getAllProducts());
            System.out.println("Product Search Button was pressed");
        }
    }

    // Modify Part Button
    @FXML
    private void updatePart(ActionEvent event) {
        if (partTableView.getSelectionModel().getSelectedItem() != null) {
            try {
                Main.handleSceneChange("ModifyPart.fxml");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    // Modify Product Button
    @FXML
    private void updateProduct(ActionEvent event) {
        if (productTableView.getSelectionModel().getSelectedItem() != null) {
            try {
                Main.handleSceneChange("ModifyProduct.fxml");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    // Delete Part Button
    @FXML
    private void deletePart(ActionEvent event) {
        if (partTableView.getSelectionModel().getSelectedItem() != null) {
            partDeleteConfirmationAlert.showAndWait();
            if (partDeleteConfirmationAlert.getResult() == ButtonType.YES) {
                System.out.println("Part: " + getSelectedPart().getName() + " deleted from Parts");
                inventory.deletePart(getSelectedPart());
            }
        }
    }

    // Delete Product Button
    @FXML
    private void deleteProduct(ActionEvent event) {
        if (productTableView.getSelectionModel().getSelectedItem() != null) {
            productDeleteConfirmationAlert.showAndWait();
            if (productDeleteConfirmationAlert.getResult() == ButtonType.YES) {
                System.out.println("Product: " + getSelectedProduct().getName() + " deleted from Products");
                inventory.deleteProduct(getSelectedProduct());
            }
        }
    }

    // Exit Button
    @FXML
    private void exitApp(ActionEvent event) {

        // If there is at least one part or product, throw a confirmation
        if (!inventory.getAllParts().isEmpty() || !inventory.getAllProducts().isEmpty()) {
            Main.exitConfirmationAlert.showAndWait();
            if (Main.exitConfirmationAlert.getResult() == ButtonType.YES) {
                Platform.exit();
            }
        } else Platform.exit();
    }

    // Initialization Method
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // Set up the Columns in the Parts Table
        partIdColumn.setCellValueFactory(new PropertyValueFactory<Part, Integer>("id"));
        partNameColumn.setCellValueFactory(new PropertyValueFactory<Part, String>("name"));
        partStockColumn.setCellValueFactory(new PropertyValueFactory<Part, Integer>("stock"));
        partPriceColumn.setCellValueFactory(new PropertyValueFactory<Part, Double>("price"));

        // Set up the Columns in the Products Table
        productIdColumn.setCellValueFactory(new PropertyValueFactory<Product, Integer>("id"));
        productNameColumn.setCellValueFactory(new PropertyValueFactory<Product, String>("name"));
        productStockColumn.setCellValueFactory(new PropertyValueFactory<Product, Integer>("stock"));
        productPriceColumn.setCellValueFactory(new PropertyValueFactory<Product, Double>("price"));

        // Convert Prices to a Currency
        NumberFormat currencyInstance = NumberFormat.getCurrencyInstance();

        partPriceColumn.setCellFactory(partPriceColumn -> new TableCell<Part, Double>() {

            @Override
            protected void updateItem(Double price, boolean empty) {
                super.updateItem(price,empty);
                if (empty) {
                    setText(null);
                } else {
                    setText(currencyInstance.format(price));
                }
            }
        });


        productPriceColumn.setCellFactory(productPriceColumn -> new TableCell<Product, Double>() {

            @Override
            protected void updateItem(Double price, boolean empty) {
                super.updateItem(price,empty);
                if (empty) {
                    setText(null);
                } else {
                    setText(currencyInstance.format(price));
                }
            }
        });

        // Add the Parts and Products to their Associated Table
        partTableView.setItems(inventory.getAllParts());
        productTableView.setItems(inventory.getAllProducts());

        // Part Table View Selection Listener
        partTableView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Part>() {
            @Override
            public void changed(ObservableValue<? extends Part> observable, Part oldValue, Part newValue) {
                if (partTableView.getSelectionModel().getSelectedItem() != null) {

                    // Set the selectedPart Variable
                    selectedPart = newValue;
                    selectedPartIndex = partTableView.getSelectionModel().getSelectedIndex();
                    System.out.println("Part: " + selectedPart.getName() + " selected");
                }
            }
        });

        // Product Table View Selection Listener
        productTableView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Product>() {
            @Override
            public void changed(ObservableValue<? extends Product> observable, Product oldValue, Product newValue) {
                if (productTableView.getSelectionModel().getSelectedItem() != null) {

                    // Set the selectedProduct Variable
                    selectedProduct = newValue;
                    selectedProductIndex = productTableView.getSelectionModel().getSelectedIndex();
                    System.out.println("Product: " + selectedProduct.getName() + " selected");
                }
            }
        });
    }
}
